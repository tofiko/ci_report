<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model
{
	//panggil nama table
	private $_table_header = "pembelian_header";
	private $_table_detail = "pembelian_detail";
	
	public function tampilDataReportPagination($perpage,$uri,$dateawal,$dateakhir)
	{
		$this->db->select('*');
		if(!empty($data_pencarian) and ($dateakhirs))
		{
			//$this->db->like('tanggal',$data_pencarian);
			$this->db->where('tanggal >=',$dateawal);
			$this->db->where('tanggal <=',$dateakhir);	
		}
		$this->db->order_by('pembelian_header.id_pembelian_h','asc');
		
		$get_data = $this->db->get($this->_table_header,$perpage,$uri);
		if($get_data->num_rows() > 0)
		{
			return $get_data->result();	
		}
		else
		{
			return null;	
		}
	}
	
	public function tombolpagination($dateawal,$dateakhir)
	{
		//cari jumlah data berdasarkan data pencarian
		$this->db->start_cache();
		$this->db->select('*');
		$this->db->from('pembelian_header');
		$this->db->join('pembelian_detail as d', 'd.id_pembelian_h = pembelian_header.id_pembelian_h');
		$this->db->join('barang', 'barang.kode_barang = d.kode_barang');
		$this->db->where('tanggal >=',$dateawal);
		$this->db->where('tanggal <=',$dateakhir);
		$this->db->stop_cache();
		
		//$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		
		//pagination limit
		$pagination['base_url'] = base_url().'report/listreport/load/';
		$pagination['total_rows'] = $hasil;
		$pagination['per_page'] = "3";
		$pagination['uri_segment'] = 4;
		$pagination['num_link'] = 2;
		
		//custom paging configuration
		$pagination['full_tag_open'] = '<div class="pagination">';
		$pagination['full_tag_open'] = '</div>';
		
		$pagination['first_link'] = 'First Page';
		$pagination['first_tag_open'] = '<span class="firstlink">';
		$pagination['first_tag_open'] = '</span>';
		
		$pagination['last_link'] = 'Last Page';
		$pagination['last_tag_open'] = '<span class="lastlink">';
		$pagination['last_tag_open'] = '</span>';
		
		$pagination['next_link'] = 'Next Page';
		$pagination['next_tag_open'] = '<span class="nextlink">';
		$pagination['next_tag_open'] = '</span>';
		
		$pagination['prev_link'] = 'Prev Page';
		$pagination['prev_tag_open'] = '<span class="prevlink">';
		$pagination['prev_tag_open'] = '</span>';
		
		$pagination['cur_tag_open'] = '<span class="curlink">';
		$pagination['cur_tag_open'] = '</span>';
		
		$pagination['num_tag_open'] = '<span class="numlink">';
		$pagination['num_tag_open'] = '</span>';
		
		$this->pagination->initialize($pagination);
		
		$hasil_pagination = $this->tampilDataReportPagination($pagination['per_page'],$this->uri->segment(4),$dateawal,$dateakhir);
		
		return $hasil_pagination;
	}

	
	/*public function rules()
	{
		return
		[
			[
			//form input
			'field' 	=> 'kode_barang',
			'label'		=> 'Kode Barang',
			'rules' 	=> 'required|max_length[5]',
			'errors'	=>	[
								'required'		=>	'Kode Barang Tidak Boleh Kosong.',
								'max_length'	=>	'Kode Barang Tidak Boleh Lebih Dari 5 Karakter.'
							]
			],
				
			[			
			'field' 	=> 'nama_barang',
			'label'		=> 'Nama Barang',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Nama Barang Tidak Boleh Kosong.'
							]
			],
			
			[
			'field' 	=> 'harga_barang',
			'label'		=> 'Harga Barang',
			'rules' 	=> 'required|numeric',
			'errors'	=>	[
								'required'		=>	'Harga Barang Tidak Boleh Kosong.',
								'numeric'		=>	'Harga Barang Harus Angka.'
							]
			],
				
			[			
			'field' 	=> 'kode_jenis',
			'label'		=> 'Kode Jenis',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Kode Jenis Tidak Boleh Kosong.'
							]
			],
				
			/*[				
			'field' 	=> 'stok',
			'label'		=> 'Stok',
			'rules' 	=> 'required',
			'errors'	=>	[
								'required'		=>	'Stok Tidak Boleh Kosong.'
							]
			]
		];	
	}
	
	public function detail($kode_barang)
	{
		$this->db->select('*');
		$this->db->where('kode_barang', $kode_barang);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save()
	{
		$data['kode_barang']	= $this->input->post('kode_barang');
		$data['nama_barang']	= $this->input->post('nama_barang');
		$data['harga_barang']	= $this->input->post('harga_barang');
		$data['kode_jenis']		= $this->input->post('kode_jenis');
		$data['flag']			= 1;
		$this->db->insert($this->_table, $data);
	}
	
	public function update($kode_barang)
	{
		$data['nama_barang']	= $this->input->post('nama_barang');
		$data['harga_barang']	= $this->input->post('harga_barang');
		$data['kode_jenis']		= $this->input->post('kode_jenis');
		$data['flag']			= 1;
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($kode_barang)
	{
		//delete from db
		$this->db->where('kode_barang', $kode_barang);
		$this->db->delete($this->_table);
	}
	
	public function updateStok($kode_barang, $qty)
	{
		$cari_stok = $this->detail($kode_barang);
		foreach ($cari_stok as $data ){
			$stok = $data->stok;
	}
		
		$jumlah_stok = $stok * $qty;
		$data_barang['stok'] = $jumlah_stok;
		
		$this->db->where('kode_barang', $kode_barang);
		$this->db->update('barang', $data_barang);
	}*/

}
