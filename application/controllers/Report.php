<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		// load model terkait
		$this->load->model("report_model");
		$this->load->model("pembelian_model");
		$this->load->model("barang_model");
		
		//load validasi
		$this->load->library("form_validation");
		
		//cek sesi login
		$user_login	= $this->session->userdata();
		if(count($user_login) <= 1)
		{
			redirect("auth/index","refresh");
		}
	}
	
	public function index()
	{
		$this->listreport();
	}
	
	public function pencarianreport()
	{
		$data['content']			=	'form/Report/Pencarian_report';
		$this->load->view('home', $data);
		//redirect("Report/index", "refresh");
	}
	
	public function listreport()
	{
		if(isset($_POST['tombol_cari']))
		{
			//'cari_data' name textbox
			$data['dateawal'] = $this->input->post('dateawal');
			$data['dateakhir'] = $this->input->post('dateakhir');
			$this->session->set_userdata('session_pencarian',$data['dateawal'],$data['dateakhir']);	
		}
		else
		{
			$data['dateawal'] = $this->session->userdata('session_pencarian');	
			$data['dateakhir'] = $this->session->userdata('session_pencarian');	
		}
		
		//$data['data_jenis_barang'] 	= $this->jenis_barang_model->tampilDataJenisBarang();
		$data['data_report'] = $this->report_model->tombolpagination($data['dateawal'],$data['dateakhir']);
		//$data['data_barang'] 		= $this->barang_model->tampilDataBarang2();
		$data['content']			=	'form/Report/Home_report';
		$this->load->view('home', $data);
	}
	
	/*public function listreport()
	{
		$data['data_pembelian'] 	= $this->pembelian_model->tampilDataPembelian();
		$data['data_pembelian_detail'] 	= $this->pembelian_model->tampilDataPembelianDetail();
		$data['data_report'] 		= $this->report_model->tombolpagination($data['dateawal'],$data['dateakhir']);
		$data['content']			=	'form/Report/Home_report';
		$this->load->view('home', $data);
	}
	
	/*public function input()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['content']			=	'form/Barang/Input_barang'; 
		
		/*if (!empty($_REQUEST)) {
			$m_barang = $this->barang_model;
			$m_barang->save();
			redirect("barang/index", "refresh");
		}
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->barang_model->rules());
		
		if ($validation->run())
		{
			$this->barang_model->save();
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Input Data Berhasil !</div>');
			redirect("Barang/index", "refresh");	
		}
		
		$this->load->view('home', $data);
	}
	
	public function detailbarang($kode_barang)
	{
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$data['content']			=	'form/Barang/Detail_barang';
		$this->load->view('home', $data);
	}
	
	public function edit($kode_barang)
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenisBarang();
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$data['content']			=	'form/Barang/Edit_barang';
		
		/*if (!empty($_REQUEST)) 
		{
			$m_barang = $this->barang_model;
			$m_barang->update($kode_barang);
			redirect("barang/index", "refresh");
		}
		
		//validasi
		$validation	= $this->form_validation;
		$validation	-> set_rules($this->barang_model->rules());
		
		if ($validation->run())
		{
			$this->barang_model->update($kode_barang);
			$this->session->set_flashdata('info','<div style="color:blue" align="center">Update Data Berhasil !</div>');
			redirect("Barang/index", "refresh");	
		}

		$this->load->view('home', $data);
	}
	
	public function delete($kode_barang)
	{
		$m_barang = $this->barang_model;
		$m_barang->delete($kode_barang);
		redirect("barang/index", "refresh");
	}*/
	
}