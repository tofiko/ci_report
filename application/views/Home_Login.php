<html>
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style/login/style.css">
</head>
<body>
    <div class="konten">
        <div class="kepala">
            <div class="lock"></div>
            <h2 class="judul">Sign In</h2>
        </div>
        <div class="artikel">
            <form action="<?=base_url()?>auth/login" method="post">
                <div class="grup">
                    <label for="email">Username</label>
                    <input type="text" name="username" id="username" placeholder="Masukkan Username Anda">
                </div>
                <div class="grup">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" placeholder="Masukkan password Anda">
                </div>
                <div class="grup">
                    <input type="submit" value="Sign In">
                </div>
                <?php
                	if ($this->session->flashdata('info') == true)
					{
						echo $this->session->flashdata('info');	
					}
				?>
            </form>
        </div>
    </div>
</body>
</html>